/**
* @file crypto.cpp
* @brief Probably going to merge this with boot.(h|cpp) in the future.
*/

using namespace std;
#include <cstring>
#include "crypto.h"
#include "boot.h"
extern "C" {
	#include "sodium.h"

}



size_t size16_t = 16;
size_t size32_t = 32;
size_t size64_t = 64;
size_t size128_t = 128;
size_t size160_t = 160;
size_t size224_t = 224;

asym_keypairs myKeys = {};
identity myIdentity = {};
packet_counter myCounter = {};
packet_counter mySavedCounter = {};



void verify_fingerprint(){
}

/**
* @brief Saves myCounter to nvs, if necessary.
* This is run every time it is called by encryptDatagram(),
* (in other words, before every packet is transmitted.)
*
* Because we can NEVER use the same counter value twice without dire security
* consequences, and we need to account for unforseen reloads, we need to make
* sure the counter value saved to NVS is always higher than the counter value in RAM.
*
* Because flash memory has a limited number of writes, we shouldn't do it very often.
* Therefore, we round the current counter up to the nearest multiple of 256, and
* save that value to nvs before our counter in RAM reaches that amount.
* TODO: The counter is a 4-byte number and the node should kill itself and start
* over from first-boot when counter reaches 4,294,967,295 to prevent counter re-use...
* Even though a node would have to transmit 10 packets per second for 13+ years to
* get there (or reboot a bunch of times).
*/

void save_myCounter(){
	// mySavedCounter should =
	packet_counter counterToSave = {};
	counterToSave.u32 = {myCounter.u32 + 256 - 1 - (myCounter.u32 - 1) % 256};
	if(counterToSave.u32 > mySavedCounter.u32){
		nvs_handle my_handle = nvs_init();
		nvs_set_blob(my_handle, "myCounter", counterToSave.u8, size32_t);
		mySavedCounter.u32 = counterToSave.u32;
	}
}

/**
* @brief Generate our identity on every boot.
* This will be sent to neighbors so that they know all our public keys, and fingerprint.
* It consists of our public signing key, public key exchange key, the signed_fingerprint,
* and the fingerprint.
* In practice, we will not need to transmit myIdentity.fingerprint, that is why it is last.
* It can be read after receiver node validates signed_fingerprint.
*/

void generate_identity(){
  uint8_t keyHash[16] = {};
  // Hash the blob of all our public keys together, it will be our fingerprint
  crypto_generichash(keyHash, size16_t,
                     myKeys.pub_blob, 64,
                     NULL, 0);
	memcpy(myIdentity.pub_sign, myKeys.pub_sign, size32_t);
	memcpy(myIdentity.pub_kx, myKeys.pub_kx, size32_t);
//memcpy(myIdentity.signed_fingerprint, sign_fingerprint(), size64_t); // Not yet implemented
	memcpy(myIdentity.fingerprint, keyHash, size16_t);
}
