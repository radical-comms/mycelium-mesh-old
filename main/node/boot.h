/**
* @file boot.h
* @brief Functions Initialization of Non-Volitile storage, and
*  for generating cryptographic keys and initial counter if this
*  is the first boot (determined by the lack of those values within
*  NVS storage.)
*/

#pragma once

#include "nvs_flash.h"
#include "nvs.h"
#include "crypto.h"

nvs_handle nvs_init();
void firstboot_keys();
void firstboot_counter();
void node_init();
