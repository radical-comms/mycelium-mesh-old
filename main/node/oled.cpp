/**
* @file oled.cpp
* @brief Functions for an OLED display
*/

using namespace std;

#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <cassert>
#include <iostream>

extern "C" {
  #include "../oled/ssd1306.h"
  #include "../oled/ssd1306_draw.h"
  #include "../oled/ssd1306_font.h"
  #include "../oled/ssd1306_default_if.h"
}



static const int I2CDisplayAddress = 0x3C;
static const int I2CDisplayWidth = 128;
static const int I2CDisplayHeight = 64;
static const int I2CResetPin = -1;
struct SSD1306_Device I2CDisplay;


bool DefaultBusInit( void ) {
        assert( SSD1306_I2CMasterInitDefault( ) == true );
        assert( SSD1306_I2CMasterAttachDisplayDefault( &I2CDisplay, I2CDisplayWidth, I2CDisplayHeight, I2CDisplayAddress, I2CResetPin ) == true );
    return true;
};

void oledSetup( struct SSD1306_Device* DisplayHandle, const struct SSD1306_FontDef* Font ) {
    SSD1306_Clear( DisplayHandle, SSD_COLOR_BLACK );
    SSD1306_SetFont( DisplayHandle, Font );
}

void oledPrint( struct SSD1306_Device* DisplayHandle, const char* text ) {
    SSD1306_Clear( DisplayHandle, SSD_COLOR_BLACK );
    SSD1306_FontDrawAnchoredString( DisplayHandle, TextAnchor_West, text, SSD_COLOR_WHITE );
    SSD1306_Update( DisplayHandle );
}
