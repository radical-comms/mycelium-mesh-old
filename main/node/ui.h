/**
* @file ui.h
* @brief User interface functionality (console output, etc)
*/
#pragma once
using namespace std;
#include <string>
using std::string;
extern struct packet p;

string uint_to_hex(uint8_t *input, uint8_t size, bool reverse = false);
void printKeys();
