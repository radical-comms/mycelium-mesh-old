/**
* @file net-core.cpp
* @brief Data structures for packets/datagrams and functions
*  for transmitting and receiving packets/datagrams, as well
*  as a decision tree-like function for passing received datagrams
*  to the proper external functions for further processing
*/

using namespace std;
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include <limits.h>
#include <iostream>		  // std::cout
#include <cstring>
#include <iostream>
#include <queue>          // std::queue
#include <string>
#include <cstdio>
#include <forward_list>
#include <map>
#include <endian.h>
#include <stdlib.h>

extern "C" {
	#include "lora.h"
	#include "sodium.h"
}
#include "config.h"
#include "../node/crypto.h"
#include "../node/ui.h"
#include "../vars.h"
#include "net-core.h"
#include "esp32/rom/crc.h"
#include "lora.h"

#define REG_PAYLOAD_LENGTH        0x22
#define REG_MODEM_STAT						0x18



queue<packet> tx_buffer;
bool txLock = false;


/**
* @brief Member function for completing all the steps necessary to send
*  a packet onto the network.
*/
void packet::send(vector<uint8_t> payload_in, uint8_t type_in, uint8_t type_ext_in){
	payload = payload_in;
	type = type_in;
	type_ext = type_ext_in;
	tx_increment_counter();
	tx_prepare_initbyte();
	tx_prepare_header();
	tx_prepare_nonce();
	tx_prepare_hash();
	tx_prepare_payload();
	show_queue_packet();
	tx_encrypt_payload();
	tx_prepare_packet();
};

/**
* @brief Member function for completing all the steps necessary to receive
*  a packet from the network, or rejecting invalid/malicious packets.
*/
void packet::receive(){
	datagram.resize(lora_read_reg(REG_PAYLOAD_LENGTH));
	lora_receive_packet(&datagram[0],lora_read_reg(REG_PAYLOAD_LENGTH));
	if(datagram.size() >= 8){
		rx_atomize_header();
		rx_extract_payload();
		rx_prepare_nonce();
		rx_reassemble_header();
		rx_verify_header_hash();
		rx_decrypt_payload();
		show_rx_packet();
	}else{
		cout << "Too small packet received!" << endl;
	}
};

/**
* @brief Member function to print a transmitted packet to the terminal in
*  a very aesthetically pleasing manner
*/
void packet::show_tx_packet(){
  cout << "\033[35m"; //Set Purple
	cout << "     init_byte 🡇   🡇 Counter    🡇 Network ID\
	         🡇 Auth-Code  🡇 Payload" << endl;
  cout << "ＴＸ  ⌧  🡆  🖧  " <<
	uint_to_hex(&init_byte, 1) << "  " <<
  uint_to_hex(counter.u8, sizeof(counter)) << "  " <<
  uint_to_hex(nid.u8, sizeof(nid)) << "  " <<
	uint_to_hex(header_hash.u8, sizeof(header_hash)) << "  " <<
	uint_to_hex(payload.data(), payload.size()) << endl << endl;
  cout << "\033[39m"; //Remove Purple
};

/**
* @brief Member function to print a queued packet to the terminal in
*  a very aesthetically pleasing manner
*/
void packet::show_queue_packet(){
	cout << "\033[35m"; //Set Purple
	cout << "     init_byte 🡇   🡇 Counter    🡇 Network ID\
	         🡇 Auth-Code  🡇 Payload" << endl;
	cout << "ＱＵＥ🖳  🡆  ⌧  " <<
	uint_to_hex(&init_byte, 1) << "  " <<
	uint_to_hex(counter.u8, sizeof(counter)) << "  " <<
	uint_to_hex(nid.u8, sizeof(nid)) << "  " <<
	uint_to_hex(header_hash.u8, sizeof(header_hash)) << "  " <<
	uint_to_hex(payload.data(), payload.size()) << endl << endl;
	cout << "\033[39m"; //Remove Purple
};

/**
* @brief Member function to print a received packet to the terminal in
*  a very aesthetically pleasing manner.
*/
void packet::show_rx_packet(){
	cout << "\033[36m"; //Set Cyan
	cout << "     init_byte 🡇   🡇 Counter    🡇 Network ID\
	         🡇 Auth-Code  🡇 Payload" << endl;
  cout << "ＲＸ  🖧  🡆  🖳  " <<
	uint_to_hex(&init_byte, 1) << "  " <<
	uint_to_hex(counter.u8, sizeof(counter)) << "  " <<
	uint_to_hex(nid.u8, sizeof(nid)) << "  " <<
	uint_to_hex(header_hash.u8, sizeof(header_hash)) << "  " <<
	uint_to_hex(payload.data(), payload.size()) << endl << endl;
  cout << "\033[39m"; //Remove Cyan
};

/**
* @brief The very first member function in the tx-chain of member functions,
*  it is vitally important that the message counter is incremented for
*  EVERY message to avoid encryption from being broken.
*/
void packet::tx_increment_counter(){
		myCounter.u32 = (myCounter.u32 + 1);
		if (myCounter.u32 >= mySavedCounter.u32){save_myCounter();}
};

/**
* @brief Like it says, this member function is responsible for assembling
*  the initbyte, using prx, proto, mc_len, nid_len values.
*/
void packet::tx_prepare_initbyte(){
	prx = 0b00000000;
	if (1 == nodeType){
		proto = 0b00010000;
	} else {
		proto = 0b00100000;
	};
	mc_len = 0b00001100;
	nid_len = 0b00000011;
	init_byte = (prx + proto + mc_len + nid_len);
};

/**
* @brief This member function is responsible for assembling
*  the beginning portion of the header using the message counter
*  and the Network ID. The Endianness is converted from the native
*  Little endian to Big endian for transmission on the network.
*/
void packet::tx_prepare_header(){
	counter.u32 = myCounter.u32;
	nid.u64 = myIdentity.fingerprint64;
	counter.u32 = htobe32(counter.u32);
	nid.u64 = htobe64(nid.u64);
	header.reserve(20);
	header.push_back(init_byte);
	if(prx != 0){header.push_back(pr_ext);};
	for(int i=0;i<=3;i++){
		header.push_back(counter.u8[i]);
	};
	for(int i=0;i<=7;i++){
		header.push_back(nid.u8[i]);
	};
};

/**
* @brief This member function assembles the nonce for later encryption
*  of the payload, using the full message counter and full Network ID.
*/
void packet::tx_prepare_nonce(){
	for(int i=0;i<=3;i++){nonce[i] = counter.u8[i];};
	for(int i=0;i<=7;i++){nonce[i+4] = nid.u8[i];};
};

/**
* @brief In this member function, the MAC- or Message Authentication Code
*  is generated by hashing the header using a secret pre-shared key as
*  the salt essentially... The hash algorithm is SipHash-2-4
*/
void packet::tx_prepare_hash(){
	uint8_t our_header_hash[8];
	crypto_shorthash(our_header_hash, header.data(), header.size(), sipKey);
	for(int i=0; i<=3; i++){
		header_hash.u8[i] = our_header_hash[i];
	};
	header_hash.u32 = htobe32(header_hash.u32);
	for(int i=0;i<sizeof(header_hash);i++){
		header.push_back(header_hash.u8[i]);
	};
};

/**
* @brief Member function to slap the type field(s) on the front of the payload
*  that is literally all it does...
*/
void packet::tx_prepare_payload(){
	if( type_ext != 0){payload.insert(payload.begin(), type_ext);};
	payload.insert(payload.begin(), type);
};

/**
* @brief It's an encryption member function.
*/
void packet::tx_encrypt_payload(){
	crypto_stream_chacha20_ietf_xor(payload.data(), payload.data(), payload.size(), nonce, key);
};

/**
* @brief It sticks the header and the payload together and calls it a "packet".
*  This is the last member function for building the packet, from here the
*  original function which called the packet class is responsible for completing
*  the final step of pushing the packet to the tx_buffer function.
*/
void packet::tx_prepare_packet(){
	datagram.reserve(header.size() + payload.size());
	datagram.insert(datagram.end(), header.begin(), header.end());
	datagram.insert(datagram.end(), payload.begin(), payload.end());
};

void packet::rx_atomize_header(){
	init_byte = datagram.front();
	prx = init_byte & 0b10000000;
	proto = init_byte & 0b01110000;
	mc_len = init_byte & 0b00001100;
	nid_len = init_byte & 0b00000011;
	prx = prx>>7;
	proto = proto>>4;
	mc_len = mc_len>>2;
	if(prx == 1){pr_ext = datagram.at(1);}
	for (int i=0; i < 4; i++){
		counter.u8[i] = datagram.at(i + 1 + prx);
	};
	switch (nid_len){
		case 0: nid_bytes = 1; break;
		case 1: nid_bytes = 2; break;
		case 2: nid_bytes = 4; break;
		case 3: nid_bytes = 8; break;
	};
	for (int i=0; i < nid_bytes; i++){
		nid.u8[i] = datagram.at(i + 2 + mc_len + prx);
	};
};

void packet::rx_extract_payload(){
	for(int i=0; i<=(datagram.size()-1-prx-(mc_len+1)-nid_bytes-4-1); i++){
		payload.push_back(datagram.at(i + 1 + prx + (1 + mc_len) + nid_bytes + 4));
	};
};

void packet::rx_prepare_nonce(){
	for(int i=0;i<=3;i++){nonce[i] = counter.u8[i];};
	for(int i=0;i<=7;i++){nonce[i+4] = nid.u8[i];};
};

void packet::rx_reassemble_header(){
	header.reserve(20);
	header.push_back(init_byte);
	if(prx != 0){header.push_back(pr_ext);};
	for(int i=0;i<=3;i++){
		header.push_back(counter.u8[i]);
	};
	for(int i=0;i<=7;i++){
		header.push_back(nid.u8[i]);
	};
};

bool packet::rx_verify_header_hash(){
	for(int i=0; i<=3; i++){
		header_hash.u8[i] = datagram.at(i + 3 + prx + mc_len + (nid_bytes - 1));
	};
	header_hash.u32 = be32toh(header_hash.u32);
	uint8_t our_header_hash[8];
	crypto_shorthash(our_header_hash, header.data(), header.size(), sipKey);
	for(int i=0; i<=3; i++){
		if(our_header_hash[i] != header_hash.u8[i]){
			if (verbose==true){
			cout << "[::rx_verify_head._hash]| Invalid header hash! " << endl;
			cout << "[::rx_verify_head._hash]| Header Hash: " <<
			uint_to_hex(header_hash.u8, sizeof(header_hash)) << endl;
			cout << "[::rx_verify_head._hash]| Our Header Hash: " <<
			uint_to_hex(our_header_hash, sizeof(our_header_hash)) << endl;
			cout << header.size() << endl;
			};
			return false;
		};
	};
return true;
};

void packet::rx_decrypt_payload(){
	crypto_stream_chacha20_ietf_xor(payload.data(), payload.data(), payload.size(), nonce, key);
};

/**
* @brief Member function which decides which external function to send
*  received payload and other information as necessary for further processing.
*/

void packet::rx_process_payload(){
	if(payload.front() == TYPE_NetMGMT_Hello){
		// Indicates a NetMGMT Hello
//		NeighborTable_Add(nid);

	}
};

/**
* @brief Function for pulling packets out of the tx_queue and
* transmitting them.
*/
void tx_transmit(){
	packet mypacket = tx_buffer.front();
	mypacket.show_tx_packet();
	txLock=true;
	if (lora_read_reg(REG_MODEM_STAT) & 1){
		cout << "[::tx_transmit]       | Waiting for a clear channel..." << endl;
		while (lora_read_reg(REG_MODEM_STAT) & 1){
			vTaskDelay(entropyQueue.front() / portTICK_PERIOD_MS);
			entropyQueue.pop();};};
	lora_send_packet(mypacket.datagram.data(), mypacket.datagram.size());
	tx_buffer.pop();
	txLock=false;
};
